# Pettan.js
Completely flat event based micro framework. Created after getting fed up with
all sorts of MVC frameworks (like Angular) that are, frankly, just not flat.

Pettan is great for fast prototyping of effective UI and dynmaics. Instead of 
all kinds of opaque bindings, models, and contrived controllers, we present a
simple cause-effect method of design. With Pettan, you simply consider two 
things: Causes (e.g. user input events, timers, responses) and Effects (update 
the UI, fire an AJAX request or Cause a new event to happen!)

Best of all, the code is super flat. No callback hell, no promises, no problems!

## Event based design
Pettan offers two main event systems that are ever so slightly different. 

- Handlers are opaque endpoints, they may invoke none or many further events, 
    which are opaque to the emitting party.
- Services are simple pipelines, they invoke one target specified by the 
    emitting party.

### Handlers
Handlers are very useful for end updates. For example, you could have a simple
UI update handler like so

    Pettan.on('ui.updateGraph', function(data) {
       graph.setDataPoints(data.dataPoints);
       // ...
    });

Then you can cause a graph update anywhere throughout your code!

    Pettan.on('form.user_supplied_form.submit', function(formData) {
       var dataPoints = SomeLibrary.packagedFormData(formData);
       
       // Cause the UI to update
       emit('ui.updateGraph', {'dataPoints':dataPoints});
       
       // Cause the data to be piped to the server through a service (see next
       // section)
       emit('ajax:HandleServerResponse:NotifyUser', dataPoints);
    });
   
    setInterval(function(){
        Pettan.emit('ui.updateGraph', 
            {'dataPoints': SomeLibrary.Collect.collectData()});
    }, 2000);
   
In fact, you can emit the message any time you want to update!

### Services
Services are slightly different in that they have one guaranteed output. 
Services may cause other events, but they must guarantee a final endpoint that
must be called at every exit condition.

Services have a unique naming scheme: `ServiceName:[...Next handler]`
Where the service name is the part before the colons and the next event comes 
after the first colon. Services are dispatched the same way as handlers, through
`emit()`-ing them. 

This has the small benefit of hacking together chained services: 
`Service1:Service2:Service3:FinalEndpoint`. Pettan will simply peel off the 
service invocations one at a time.

## Reflecting
Lots of things in JavaScript create events. A very interesting way to bind them
to pettan is to `.reflect` them. This creates a listener on the foreign event
and binds it to some local event.

    Pettan.reflect('load', 'myloadEvent', window);
    
Pettan supports the `on('eventName', callback)` and 
`addEventListener('eventName', callback)` methods. It will also check the 
existance of `[eventName](callback)` and try binding there. 

## Concurrency
or lack thereof. Pettan does not handle concurrency at all. But, umm, we provide
a tiny helper that will allow you to hopefully handle it better. 

    Pettan.acquireAtom()
    
returns a globally unique number every time it is called. 

It's not much, huh. That's where you can use the inefficiencies of JS. JS is 
single threaded, so that means:

    Pettan.emit('A', ...);
    Pettan.emit('B', ...);
    
will always result in the entire handle chain for A executed before the chain
for B starts. There are caveats to this approach, for timer events, ajax 
requests and such are actually asynchronous. Handling those well is left as an
exercise to the reader.

# But wait...
... isn't this just global methods?

Maybe...
