// Pettan.js - Basic Services - Author: Jim Chen - Licensed under the MIT License
(function(Pettan){
    var AjaxException = function(){};
    Pettan.service('ajax', function(request, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function() {
           if (xhr.readyState == 4) {
               callback(xhr.responseText);
           }
        };
        xhr.open(request.method, request.url, true);
        xhr.send(request.body);
    });
})(Pettan);
