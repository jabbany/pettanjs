# Using Pettan Design to Calculate the Fibonacci Sequence (Inefficiently)
This is a bad example, since Pettan is designed to abstract out communication
not business logic. Business logic should be abstracted on its own as blocks.

    // Closed environment with shared globals for calculating the fibonacci sequence
    (function(finishCalculationCallback){
      var total = 0;

      Pettan.on('collectResults', function(result) {
        if (result.isEnd) {
          finishCalculationCallback(total);
          total = 0;
          return;
        }
        total += result.value;
      });

      Pettan.service('fib', function(num, callback) {
        if (num.value <= 1) {
          callback({
              'isEnd': false,
              'value': num.value
            });
          return;
        }
        Pettan.emit('fib:' + num.collector, {
          'collector': num.collector, 
          'value':num.value - 1
        });
        Pettan.emit('fib:' + num.collector, {
          'collector': num.collector, 
          'value':num.value - 2
        });
        // Needed to make this a proper service
        callback({
            'isEnd': false,
            'value': 0
          });
      });

      Pettan.on('calculateFib', function(iter) {
        if (typeof iter === 'object') {
          var collector = iter.collector;
          var start = iter.value;
        } else {
          var collector = 'collectResults';
          var start = iter;
        }
        Pettan.emit('fib:' + collector, {
          'collector': collector,
          'value': start
        });
        // We can do this since JS is synchronous
        Pettan.emit('collectResults', {'isEnd': true});
      });
    })(function(result){
      console.log(result);
    });

We can calculate the sequence very simply by using 

    Pettan.emit('calculateFib', 11);
    
Also, this allows us to define custom transports, so we could add a logging 
service like so :

    var pf_callCount = 0; Pettan.service('fibProfile', function(data, cb){pf_callCount += 1; cb(data);});
    
And then:

    Pettan.emit('calculateFib', {'value': 11, 'collector': 'fibProfile:collectResults');
    // Inspect pf_callCount to see how many calls the service made :) lol
    
## A much better example of the Fibonacci service (And services in general)

    Pettan.service('fib', function(val, cb) {
      var fibA = 0, fibB = 1;
      for(var i = 0; i < val; i++) {
        var tmp = fibA + fibB;
        fibA = fibB;
        fibB = tmp;
      }
      cb(fibA);
    });
    
    Pettan.service('prettyPrint', function(val, cb) {
      var pretty = "";
      var gd = function(num, digits) {
        var st = "" + num;
        while(st.length < digits){
          st = "0" + st;
        }
        return st;
      };
      while(val >= 1000){
        pretty = ("," + gd(val % 1000, 3)) + pretty;
        val = Math.floor(val/1000);
      }
      pretty = "" + val + pretty;
      cb(pretty);
    });
    
    Pettan.on('logAnswer', function(answer){
      console.log(answer);
    });
    
    Pettan.emit('fib:prettyPrint:logAnswer', 66);
