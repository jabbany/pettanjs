// Pettan.js - Author: Jim Chen - Licensed under the MIT License
var Pettan = (function(){
    var ServiceException = function(name, ex){
        this.name = name;
        this.exception = ex;
        if (ex != null) {
          this.stack = ex.stack;
        }
    };
    ServiceException.prototype.toString = function() {
        return "ServiceException : Service[" + this.name + "]\n" + 
            this.exception;
    };
    var Logger = console != null ? console : {
            "log": function(){}, 
            "error": function(){}, 
            "warn": function(){}
        };

    var Pettan = new function(){
        var services = {};
        var bindings = {};
        var held = {};
        var atom = 0;

        this.emit = function(eventName, data) {
            var serviceName = eventName.split(":").shift();
            if (held.hasOwnProperty(eventName)) {
                Logger.log('Holding event ' + eventName + ' in pool of ' + 
                    held[eventName].length + ' held events.');
                held[eventName].push(data);
            } else if (services.hasOwnProperty(serviceName)) {
                var callbackName = eventName.split(":").slice(1).join(":");
                try{
                    services[serviceName](data, (function(result){
                        this.emit(callbackName, result);
                    }).bind(this));
                } catch (e) {
                    Logger.error(e);
                }
            } else if (bindings.hasOwnProperty(eventName)) {
                for (var i = 0; i < bindings[eventName].length; i++) {
                    try{
                        bindings[eventName][i](data);
                    } catch (e) {
                        Logger.error(e);
                    }
                }
            } else {
                Logger.warn("Unrecognized endpoint \"" + eventName + "\".");
            }
            return this;
        };

        this.on = function(eventName, callback) {
            if (!bindings.hasOwnProperty(eventName)) {
                bindings[eventName] = [];
            }
            bindings[eventName].push(callback);
            return this;
        };

        this.off = function(eventName) {
            delete bindings[eventName];
            return this;
        };

        this.reflect = function(eventName, sourceEventName, source) {
            source.addEventListener(sourceEventName, (function(event){
                this.emit(eventName, event);
            }).bind(this));
            return this;
        };

        this.service = function(serviceName, service) {
            services[serviceName] = function(data, callback){
                try{
                    service(data, callback);
                } catch(e) {
                    callback(new ServiceException(serviceName, e));
                }
            };
            return this;
        };

        this.hold = function(eventName) {
            held[eventName] = [];
            return this;
        };

        this.release = function(eventName) {
            if (typeof held[eventName] !== "object") {
                Logger.warn('Releasing unheld event ' + eventName);
                return this;
            }
            var prepare = [];
            for (var i = 0; i < held[eventName].length; i++) {
                prepare.push(held[eventName][i]);
            }
            delete held[eventName];
            for (var i = 0; i < prepare.length; i++) {
                this.emit(eventName, prepare[i]);
            }
            Logger.log('Released ' + prepare.length + ' held events for ' + eventName);
            return this;
        };

        this.after = function(timer, eventName, data) {
            setTimeout((function(){
              this.emit(eventName, data);
            }).bind(this), timer);
            return this;
        };

        this.every = function(timer, eventName, data) {
            setInterval((function(){
              this.emit(eventName, data);
            }).bind(this), timer);
            return this;
        };

        this.acquireAtom = function() {
            return atom++;
        };
    };
    return Pettan;
})()
